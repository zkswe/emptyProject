rmdir sdcard
mkdir sdcard
cd sdcard
mkdir ui
mkdir lib
cd ..
xcopy %cd%\ui\Resources %cd%\sdcard\ui /d /y /e /h
copy ui\build\*.ftu sdcard\ui\
copy ui\build\EasyUIdebug.cfg sdcard\EasyUI.cfg
copy src\libs\armeabi\libzkgui.so sdcard\lib\libzkgui.so
pause 