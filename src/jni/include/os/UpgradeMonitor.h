/*
 * UpgradeMonitor.h
 *
 *  Created on: Aug 4, 2017
 *      Author: guoxs
 */

#ifndef _OS_UPGRADE_MONITOR_H_
#define _OS_UPGRADE_MONITOR_H_

#include <string>
#include <vector>
#include "MountMonitor.h"

class UpgradeMonitor : Thread {
public:
	virtual ~UpgradeMonitor();

	void startMonitoring();
	void stopMonitoring();

	bool startUpgrade();
	void stopUpgrade();

	bool isUpgrading() const;
	bool needUpgrade() const;

	bool checkUpgrade();

	static UpgradeMonitor* getInstance();

public:
	typedef unsigned char BYTE;

	typedef enum {
		E_UPGRADE_STATUS_START,
		E_UPGRADE_STATUS_SUCCESS,
		E_UPGRADE_STATUS_ERROR
	} EUpgradeStatus;

	typedef enum {
		E_UPGRADE_TYPE_UBOOT,
		E_UPGRADE_TYPE_BOOT,
		E_UPGRADE_TYPE_SYSTEM,
		E_UPGRADE_TYPE_RES,
		E_UPGRADE_TYPE_DATA,
		E_UPGRADE_TYPE_LOGO,
		E_UPGRADE_TYPE_ENV,
		E_UPGRADE_TYPE_PRIVATE,
		E_UPGRADE_TYPE_FULL
	} EUpgradeType;

	/**
	 * 分区信息
	 */
	typedef struct {
		BYTE partn;			// 分区号
		BYTE reserve[3];	// 保留
		BYTE offset[4];		// 基于head的偏移
		BYTE imageSize[4];	// 分区镜像大小
		BYTE imageHead[16];	// image头数据 swap MD5
	} SPartInfo;

	typedef struct {
		EUpgradeType upgradeType;
		std::string upgradeFilePath;
		std::string partName;
		SPartInfo partInfo;
		bool needUpgrade;
	} SUpgradeInfo;

	class IUpgradeStatusListener {
	public:
		virtual ~IUpgradeStatusListener() { };
		virtual void notify(int what, int status, const char *msg) = 0;
	};

	void setUpgradeStatusListener(IUpgradeStatusListener *pListener) {
		mUpgradeStatusListenerPtr = pListener;
	}

	std::vector<SUpgradeInfo*>& getUpgradeInfoList() {
		return mUpgradeInfoList;
	}

protected:
	virtual bool threadLoop();

private:
	UpgradeMonitor();

	bool checkUpgradeFile(const char *pPath);
	bool upgradeFile(const SUpgradeInfo &upgradeInfo);
	bool checkMD5(FILE *pf, const SPartInfo &partInfo);
	void clearUpgradeInfo();

	void initMtdInfoList();
	const char* getMtdDevByName(const char *pName) const;
	bool writeMtdDev(const char *pDev, FILE *pf, const SUpgradeInfo &upgradeInfo);

private:
	class UpgradeMountListener : public MountMonitor::IMountListener {
	public:
		UpgradeMountListener(UpgradeMonitor *pUM) : mUMPtr(pUM) {

		}

		virtual void notify(int what, int status, const char *msg) {
			switch (status) {
			case MountMonitor::E_MOUNT_STATUS_MOUNTED:
				mUMPtr->checkUpgradeFile(msg);
				break;

			case MountMonitor::E_MOUNT_STATUS_REMOVE:
				break;
			}
		}

	private:
		UpgradeMonitor *mUMPtr;
	};

private:
	UpgradeMountListener mUpgradeMountListener;
	IUpgradeStatusListener *mUpgradeStatusListenerPtr;

	bool mHasStartMonitor;

	std::vector<SUpgradeInfo*> mUpgradeInfoList;
	std::string mMountPath;

	bool mIsUpgrading;

	typedef struct {
		std::string dev;
		std::string name;
	} SMtdInfo;
	std::vector<SMtdInfo> mMtdInfoList;
};

#define UPGRADEMONITOR		UpgradeMonitor::getInstance()

#endif /* _OS_UPGRADE_MONITOR_H_ */
