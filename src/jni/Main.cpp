/*
 * Main.cpp
 *
 *  Created on: May 9, 2017
 *      Author: guoxs
 */
#include "entry/EasyUIContext.h"
#include "uart/UartContext.h"
#include "uart/ProtocolSender.h"
#include "uart/ProtocolParser.h"
#include "uart/ProtocolData.h"
#include "utils/Log.h"
#include "manager/ConfigManager.h"

#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */


void onEasyUIInit(EasyUIContext *pContext) {
	LOGD("onInit\n");
	UARTCONTEXT->openUart(CONFIGMANAGER->getUartName().c_str(), CONFIGMANAGER->getUartBaudRate());
}

void onEasyUIDeinit(EasyUIContext *pContext) {
	LOGD("onDestroy\n");
	UARTCONTEXT->closeUart();
}

const char* onStartupApp(EasyUIContext *pContext) {
	return "LauncherActivity";
}

#ifdef __cplusplus
}
#endif  /* __cplusplus */
