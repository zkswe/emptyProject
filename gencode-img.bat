@echo off
rmdir imgout
mkdir imgout
cd imgout
mkdir ui
mkdir lib
mkdir etc
cd ..
xcopy %cd%\ui\Resources %cd%\imgout\ui /d /y /e /h
copy ui\build\*.ftu imgout\ui\
copy ui\build\EasyUIimg.cfg imgout\etc\EasyUI.cfg
copy src\libs\armeabi\libzkgui.so imgout\lib\libzkgui.so

imagemaker32
echo %errorlevel%

if %errorlevel% == 0 ( 
echo success 
) else (
echo failed!  
)

echo press any key exit
pause >nul